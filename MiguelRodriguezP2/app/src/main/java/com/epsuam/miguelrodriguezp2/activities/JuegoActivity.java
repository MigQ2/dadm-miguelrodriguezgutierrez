package com.epsuam.miguelrodriguezp2.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.epsuam.miguelrodriguezp2.R;
import com.epsuam.miguelrodriguezp2.model.MovimientoConecta4;
import com.epsuam.miguelrodriguezp2.model.TableroConecta4;
import com.epsuam.miguelrodriguezp2.view.OnPlayListener;
import com.epsuam.miguelrodriguezp2.view.TableroConecta4View;

import java.util.ArrayList;

import es.uam.eps.multij.AccionMover;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.JugadorAleatorio;
import es.uam.eps.multij.Partida;
import es.uam.eps.multij.Tablero;

public class JuegoActivity extends AppCompatActivity implements Jugador, OnPlayListener {

    private Partida partida;
    private TableroConecta4View tableroview;
    TextView infotext;
    ArrayList<Jugador> jugadores;
    Tablero tablero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        infotext = (TextView) findViewById(R.id.infotext);
        JugadorAleatorio jugadorAleatorio = new JugadorAleatorio("Máquina");
        jugadores = new ArrayList<Jugador>();
        jugadores.add(JuegoActivity.this);
        jugadores.add(jugadorAleatorio);
        tablero=new TableroConecta4();
        if(savedInstanceState!=null){
            try {
                tablero.stringToTablero(savedInstanceState.getString("tablero"));
            } catch (ExcepcionJuego excepcionJuego) {
                Toast.makeText(tableroview.getContext(), "ERROR: Excepcion al recuperar el tablero de onCreate(Bundle savedInstanceState)", Toast.LENGTH_LONG).show();
            }
        }




        tableroview = (TableroConecta4View) findViewById(R.id.tableroc4);
        tableroview.setOnPlayListener(this);
        tableroview.setTablero((TableroConecta4) tablero);
        partida	= new Partida(tablero, jugadores);
        if(tablero.getEstado()==Tablero.EN_CURSO){
            partida.comenzar(tablero, jugadores);

        }

        if(tablero.getEstado()==Tablero.TABLAS){
            infotext.setText("Tablas.");
        }
        if(tablero.getEstado()==Tablero.FINALIZADA) {
            infotext.setText("Gana: "+ jugadores.get(tablero.getTurno()).getNombre());
        }

        }

    @Override
    public String getNombre() {
        return "Jugador Humano";
    }

    @Override
    public boolean puedeJugar(Tablero tablero) {
        return true;
    }

    @Override
    public void onCambioEnPartida(Evento evento) {
        switch(evento.getTipo()){
            case Evento.EVENTO_CAMBIO:
                tableroview.invalidate();
                break;
            case Evento.EVENTO_CONFIRMA:
                break;
            case Evento.EVENTO_ERROR:
                break;
            case Evento.EVENTO_TURNO:
                tableroview.invalidate();
                break;
            case Evento.EVENTO_FIN:
                tableroview.invalidate();
                if(tablero.getEstado()==Tablero.TABLAS){
                    infotext.setText("Tablas.");
                }
                if(tablero.getEstado()==Tablero.FINALIZADA) {
                    infotext.setText("Gana: "+ jugadores.get(tablero.getTurno()).getNombre());
                }
                break;

        }
        return;
    }

    @Override
    public void onPlay(int columna) {
        try {
           partida.realizaAccion(new AccionMover(this, new MovimientoConecta4(columna)));
        }
        catch(Exception e) {
            Toast.makeText(tableroview.getContext(), "ERROR: Excepcion en onPlay realizaAccion", Toast.LENGTH_LONG).show();
            return;
        }
        return;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tablero",tablero.tableroToString());
        return;
    }
}
