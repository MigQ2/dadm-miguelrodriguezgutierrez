package com.epsuam.miguelrodriguezp2.model;

import es.uam.eps.multij.Movimiento;

public class MovimientoConecta4 extends Movimiento {

	Integer col;
	
	public MovimientoConecta4(int col) {
		this.col=col;
	}
	
	@Override
	public String toString() {
		return Integer.toString(col);
	}

	@Override
	public boolean equals(Object o) {
		return o.toString().equals(this.col);
	}

}
