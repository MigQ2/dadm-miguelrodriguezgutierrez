package com.epsuam.miguelrodriguezp2.model;

import java.util.ArrayList;

import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Tablero;

public class TableroConecta4 extends Tablero {

	public static final int NCOLS=7;
	public static final int NFILAS=6;
	public static final int VACIO=0;
	public static final int FICHAJ1=1;
	public static final int FICHAJ2=2;
	private int tablero[][]; //Formato [fila][columna]
	
	
	//Las fichas de jugador i se represenrtan por el entero i+1
	public TableroConecta4() {
		turno=0;
		estado=EN_CURSO;
		
		
		tablero = new int[NFILAS][];
		for(int i=0;i<NFILAS;i++){
			tablero[i] = new int[NCOLS];
		}
		
	}

	public int getFicha(int fila, int col) {
		return tablero[fila][col];

	}

	@Override
	public void mueve(Movimiento m) throws ExcepcionJuego {
		int i;
		if(!esValido(m)){
			throw new ExcepcionJuego("Jugada ilegal");
		}
		for( i=0;i<NFILAS;i++){
			if(tablero[i][Integer.parseInt(m.toString())] == VACIO){
				tablero[i][Integer.parseInt(m.toString())] = turno+1;
				break;
			}
		}
		
		actualizarEstado(i, Integer.parseInt(m.toString()));
		numJugadas++;
		ultimoMovimiento=m;
		if(estado!=EN_CURSO){
			return;
		}
		turno=(turno+1)%numJugadores;
		return;
	}

	@Override
	public boolean esValido(Movimiento m) {
		if(tablero[NFILAS-1][Integer.parseInt(m.toString())] != VACIO){
			return false;
		}	
		else{
			return true;
		}
	}

	@Override
	public ArrayList<Movimiento> movimientosValidos() {
		ArrayList<Movimiento> arr=new ArrayList<>();
		for(int i=0;i<NCOLS;i++){
			if(tablero[NFILAS-1][i] == VACIO){
				arr.add(new MovimientoConecta4(i));
			}
		}
		return arr;
	}

	@Override
	public String tableroToString() {
	
	    String tab="";
	    tab=tab+turno+"_";
	    tab=tab+estado+"_";
	    tab=tab+numJugadas+"_";
	    tab=tab+ultimoMovimiento+"_";
	    for(int i=0;i<NFILAS;i++){
	    	for(int j=0;j<NCOLS;j++){
	    		tab=tab+tablero[i][j];	
	    	}
	    }
	    
	   

		
		
		return tab;
	}

	@Override
	public void stringToTablero(String cadena) throws ExcepcionJuego {
		String[] parts = cadena.split("_");
		turno = Integer.parseInt(parts[0]);
		estado = Integer.parseInt(parts[1]);
		numJugadas = Integer.parseInt(parts[2]);
		ultimoMovimiento = new MovimientoConecta4(Integer.parseInt(parts[3]));
		String tab=parts[4];
		for(int i=0;i<NFILAS;i++){
	    	for(int j=0;j<NCOLS;j++){
	    		tablero[i][j]=Character.getNumericValue(tab.charAt((i*NCOLS)+j));	
	    	}
	    }
		return;

	}

	@Override
	public String toString() {
		String tab="";
		for(int f=NFILAS-1;f>=0;f--){
			tab=tab+"|";
			for(int c=0; c<NCOLS;c++){
				tab=tab+numAFicha(tablero[f][c])+"|";
			}
			tab=tab+"\n";
		}
		return tab;
	}

	//Devuelvge el simbolo correspondiente a una determinada ficha
	public String numAFicha(int num){
		if(num==VACIO){
			return " ";
		}
		if(num==1){
			return "x";
		}
		if(num==2){
			return "o";
		}else{
			return "???";
		}
	}
	
	
	
	
	//Comprueba si la partida ha terminado al haber hecho el movimiento fila columna.
	//(El movimiento ya debe estar aplicado al tablero)
	//Actualiza el estado del tablero
	public void actualizarEstado(int fila, int col){
		int colorFicha=tablero[fila][col];
		int i,j;
		
		
		
		//Abajo y arriba
		int racha=1;
		for( i=fila-1;i>=0;i--){
			if(tablero[i][col]==colorFicha){
				racha++;
			}else{
				break;
			}
		}
		for( i=fila+1;i<NFILAS;i++){
			if(tablero[i][col]==colorFicha){
				racha++;
			}else{
				break;
			}
		}
		if(racha>=4){
			estado=FINALIZADA;
			if(numJugadas+1==NFILAS*NCOLS){
				estado=TABLAS;
				return;
			}
			return;
		}
		
		//Izq y dcha
		racha=1;
		for( i=col-1;i>=0;i--){
			if(tablero[fila][i]==colorFicha){
				racha++;
			}else{
				break;
			}
		}
		for( i=col+1;i<NCOLS;i++){
			if(tablero[fila][i]==colorFicha){
				racha++;
			}else{
				break;
			}
		}
		if(racha>=4){
			estado=FINALIZADA;
			if(numJugadas+1==NFILAS*NCOLS){
				estado=TABLAS;
				return;
			}
			return;
		}
		
		//Diag principal
		racha=1;
		
		for( i=col-1, j=fila-1;i>=0 && j>=0;i--,j--){
			if(tablero[j][i]==colorFicha){
				racha++;
			}else{
				break;
			}
		}
		for( i=col+1, j=fila+1;i<NCOLS && j<NFILAS;i++, j++){
			if(tablero[j][i]==colorFicha){
				racha++;
			}else{
				break;
			}
		}
		if(racha>=4){
			estado=FINALIZADA;
			if(numJugadas+1==NFILAS*NCOLS){
				estado=TABLAS;
				return;
			}
			return;
		}
		
		//Diag secundaria

		racha=1;
		
		for( i=col+1, j=fila-1;i<NCOLS && j>=0;i++,j--){
			if(tablero[j][i]==colorFicha){
				racha++;
			}else{
				break;
			}
		}
		for( i=col-1, j=fila+1;i>=0 && j<NFILAS;i--, j++){
			if(tablero[j][i]==colorFicha){
				racha++;
			}else{
				break;
			}
		}
		if(racha>=4){
			estado=FINALIZADA;
			if(numJugadas+1==NFILAS*NCOLS){
				estado=TABLAS;
				return;
			}
			return;
		}
		return;
		
	}
	
}
