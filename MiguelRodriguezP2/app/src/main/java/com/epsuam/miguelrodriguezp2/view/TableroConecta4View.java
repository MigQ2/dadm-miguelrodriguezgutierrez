package com.epsuam.miguelrodriguezp2.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.epsuam.miguelrodriguezp2.R;
import com.epsuam.miguelrodriguezp2.model.MovimientoConecta4;
import com.epsuam.miguelrodriguezp2.model.TableroConecta4;

import es.uam.eps.multij.Partida;
import es.uam.eps.multij.Tablero;


public class TableroConecta4View extends View {

    private int widthOfBoard;
    private int heightOfBoard;
    private float widthOfTile;
    private float heightOfTile;
    private Paint circlePaint;
    private Paint backgroundPaint;
    private TableroConecta4 tablero;

    private OnPlayListener playlistener;

    public TableroConecta4View(Context context, AttributeSet attributes) {
        super(context, attributes);
        setFocusable(true);
        setFocusableInTouchMode(true);
        //Partida game = new Partida();
        circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        circlePaint.setStrokeWidth(2);
        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(getResources().getColor(R.color.color_tablero));

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {


        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width = widthSize;
        int height = heightSize;

        if (widthSize < heightSize)
            height = widthSize;
        else
            width = heightSize;

        //MUST CALL THIS
        setMeasuredDimension(width, height);
    }

    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        if (width < height)
            height = width;
        else
            width = height;
        widthOfBoard = width;
        heightOfBoard = height;
        widthOfTile = width / TableroConecta4.NCOLS;
        heightOfTile = height / TableroConecta4.NFILAS;
        super.onSizeChanged(width, height, oldWidth, oldHeight);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(0, 0, widthOfBoard, heightOfBoard, backgroundPaint);
        drawTiles(canvas, circlePaint);
    }

    private void drawTiles(Canvas canvas, Paint paint) {
        float centerX, centerY, radio;
        if (widthOfBoard < heightOfBoard)
            radio = widthOfBoard / 20;
        else
            radio = heightOfBoard / 20;
        for (int i = 0; i < TableroConecta4.NFILAS; i++) {
            centerY = heightOfTile * (1 + 2 * (TableroConecta4.NFILAS-i-1)) / 2f;
            for (int j = 0; j < TableroConecta4.NCOLS; j++) {
                paint.setColor(getResources().getColor(R.color.color_error));
                if (tablero.getFicha(i, j) == TableroConecta4.VACIO) {
                    paint.setColor(getResources().getColor(R.color.color_fichavacia));
                } else if (tablero.getFicha(i, j) == TableroConecta4.FICHAJ1) {
                    paint.setColor(getResources().getColor(R.color.color_fichaj1));
                } else if (tablero.getFicha(i, j) == TableroConecta4.FICHAJ2) {
                    paint.setColor(getResources().getColor(R.color.color_fichaj2));
                }
                centerX = widthOfTile * (1 + 2 * j) / 2f;
                canvas.drawCircle(centerX, centerY, radio, paint);
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {

        //Toast.makeText(this.getContext(), "Prueba Ontouchevent", Toast.LENGTH_LONG).show();
        int xIndex = 0;
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
            //case MotionEvent.ACTION_UP:
                float x = event.getX();
                float y = event.getY();
                xIndex = xToIndex(x);
                if (xIndex < 0)
                    return false;
                if(tablero.getEstado()!=Tablero.EN_CURSO){
                    return false;
                }
                if (!tablero.esValido(new MovimientoConecta4(xIndex))) {
                    Toast.makeText(this.getContext(), "Movimiento no válido", Toast.LENGTH_SHORT).show();
                    return false;
                } else {
                    playlistener.onPlay(xIndex);
                    return super.onTouchEvent(event);
                }
        }
                return super.onTouchEvent(event);

    }

    private int xToIndex(float x) {
        return x < TableroConecta4.NCOLS * widthOfTile ? (int) (x / widthOfTile) : -1;
    }

    private int yToIndex(float y) {
        return y < TableroConecta4.NFILAS * heightOfTile ? (int) (y / heightOfTile) : -1;
    }

    public void setTablero(TableroConecta4 t) {
        tablero = t;
        return;
    }

    public void setOnPlayListener(OnPlayListener listener) {
        playlistener = listener;
        return;
    }

}
